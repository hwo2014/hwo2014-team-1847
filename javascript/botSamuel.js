var fs = require('fs');
var net = require("net");
var JSONStream = require('JSONStream');

var pistaNome = "keimola";

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

//Array com o retorno do servidor na mensagem carPositions (https://helloworldopen.com/techspec#5-server-sends-carpositions-)
var posicoesCarros = new Array();
//Retorno do servidor na mensagem gameInit (https://helloworldopen.com/techspec#3-server-sends-gameinit-)
var corrida = null;

var seguimentosCorrida = new Array();

//Array que guarda a pista que cada carro est� em cada retorno do servidor de carPositions
var trilhasCarros = new Array();

var aceleracaoPista = 1;
var friccao = 1;

/***************************************************************************/

cliente1 = net.connect(serverPort, serverHost, function () {
    return send1({
        msgType: "createRace",
        data:
        {
            botId:
            {
                name: botName,
                key: botKey
            },
            trackName: pistaNome,
            carCount: 1
        }
    });
});

function send1(json) {

    cliente1.write(JSON.stringify(json));
    return cliente1.write('\n');
};

jsonStream1 = cliente1.pipe(JSONStream.parse());

jsonStream1.on('error', function () {
    return console.log("disconnected server");
});


jsonStream1.on('data', function (data) {


    send1({
        msgType: "ping",
        data: {}
    });
});

/***************************************************************************/

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
    return send({
        msgType: "joinRace",
        data:
        {
            botId:
            {
                name: botName,
                key: botKey
            },
            trackName: pistaNome,
            carCount: 1
        }
    });
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var segumentoTipoTemp = "";
jsonStream.on('data', function (data) {

    if (data.msgType === 'carPositions') {

        posicoesCarros[posicoesCarros.length] = data;

        populaTrilhasCarros(data);

        //controlaTrocaPista()

        controlaVelocidade();



        if (segumentoTipoTemp != retornaSeguimento(0).tipo) {
            segumentoTipoTemp = retornaSeguimento(0).tipo;
            console.log(segumentoTipoTemp);
        }
        //console.log(calculaVelocidade(botName));


        //escrever( retornaQuantoFaltaParaProximoSeguimento() + ";" + calculaVelocidade() );


    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'gameStart') {

            var angulo = 0;
            for (index = 0; index < corrida.track.pieces.length; ++index) {

                if (corrida.track.pieces[index].angle != undefined)
                    angulo += corrida.track.pieces[index].angle

            }

            if (angulo > 0) {
                send({
                    msgType: "switchLane",
                    data: "Right"
                });
            }
            else {
                send({
                    msgType: "switchLane",
                    data: "Left"
                });
            }

            console.log('Race started');
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
        } else if (data.msgType === 'crash') {
            console.log('Crash');
        } else if (data.msgType === 'spawn') {
            console.log('Spawn');
        } else if (data.msgType === 'lapFinished') {
            console.log('Lap Finished');
        } else if (data.msgType === 'gameInit') {
            console.log(data);

            corrida = data.data.race;
            console.log('Game Init');


            populaSeguimentosCorrida();

        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});


function controlaTrocaPista() {

    var posicaoAtual = retornaPosicaoCarroPorNome(posicoesCarros[posicoesCarros.length - 1].data, botName);

    var indicePecaAtual = posicaoAtual.piecePosition.pieceIndex;;
    var proximaPecaComCurva;
    var proximaPecaComCurva1;
    var proximaPecaComCurva2;
    var proximaPecaComCurva3;
    var angulo = 0;


    for (index = indicePecaAtual + 1; index < corrida.track.pieces.length; ++index) {

        if (corrida.track.pieces[index].angle != undefined) {
            if (proximaPecaComCurva == undefined) {
                proximaPecaComCurva = corrida.track.pieces[index];
            } else if (proximaPecaComCurva1 == undefined) {
                proximaPecaComCurva1 = corrida.track.pieces[index];
            } else if (proximaPecaComCurva2 == undefined) {
                proximaPecaComCurva2 = corrida.track.pieces[index];
            } else if (proximaPecaComCurva3 == undefined) {
                proximaPecaComCurva3 = corrida.track.pieces[index];
            }
        }

        if (proximaPecaComCurva != undefined && proximaPecaComCurva1 != undefined && proximaPecaComCurva2 != undefined && proximaPecaComCurva3 != undefined) {
            break;
        }

    }

    if (proximaPecaComCurva != undefined && proximaPecaComCurva1 != undefined && proximaPecaComCurva2 != undefined && proximaPecaComCurva3 != undefined) {

        angulo += proximaPecaComCurva.angle + proximaPecaComCurva1.angle + proximaPecaComCurva2.angle + proximaPecaComCurva3.angle;

        if (angulo == 90) {
            send({
                msgType: "switchLane",
                data: "Right"
            });
            console.log(posicoesCarros[posicoesCarros.length - 1].gameTick + "- Direita - " + proximaPecaComCurva.angle + proximaPecaComCurva1.angle);
        }
        else if (angulo == -90) {
            send({
                msgType: "switchLane",
                data: "Left"
            });
            console.log(posicoesCarros[posicoesCarros.length - 1].gameTick + "- Esquerda - " + proximaPecaComCurva.angle + proximaPecaComCurva1.angle);
        }
    }

}

function controlaVelocidade() {

    var potencia = 0.0;
    var velocidadeDesejada = 0.0;
    var seguimentoAtual = retornaSeguimento(0);
    var proximoSeguimento = retornaSeguimento(1);

    var velocidade = calculaVelocidade(botName);

    var porcentagemPercorrida = retornaPorcentagemPercorridaSeguimento();

    var distanciaParaProximoSeguimento = retornaQuantoFaltaParaProximoSeguimento();

    if (seguimentoAtual.tipo == "reta") {

        if (seguimentoAtual.pecas.length > 2) {
            var distanciaDesacelerar = 600;

            if (proximoSeguimento.tipo == "reta")
                distanciaParaProximoSeguimento += proximoSeguimento.distancia;
            else
                distanciaDesacelerar = (600 * retornaVelocidadeDesejadaCurva(proximoSeguimento)) / 7.5;



            if (distanciaParaProximoSeguimento > distanciaDesacelerar)
                velocidadeDesejada = 10;
            else if (distanciaParaProximoSeguimento > distanciaDesacelerar - 100)
                velocidadeDesejada = 9;
            else if (distanciaParaProximoSeguimento > distanciaDesacelerar - 200)
                velocidadeDesejada = 8;
            else
                velocidadeDesejada = 7;
        } else {
            velocidadeDesejada = retornaVelocidadeDesejadaCurva(proximoSeguimento);
        }

    } else {

        velocidadeDesejada = retornaVelocidadeDesejadaCurva(seguimentoAtual);

        if (proximoSeguimento.tipo == "reta" && proximoSeguimento.pecas.distancia >= 500) {

            if (distanciaParaProximoSeguimento < 200)
                velocidadeDesejada = 8;
            else if (distanciaParaProximoSeguimento < 100)
                velocidadeDesejada = 10;

        }

    }



    if (velocidade < velocidadeDesejada) {
        if (velocidade == 0)
            potencia = 1;
        else
            potencia = (velocidadeDesejada * ((velocidade / velocidadeDesejada) + 1)) / 10;
    }

    if (potencia > 1)
        potencia = 1;

    console.log("Potencia " + potencia);
    console.log("velocidadeDesejada " + velocidadeDesejada);
    console.log("velocidade " + velocidade);

    send({
        msgType: "throttle",
        data: potencia
    });

}


function retornaVelocidadeDesejadaCurva(seguimento) {

    var velocidadeDesejada = 6.5;

    if (seguimento.raioMedio <= 30)
        velocidadeDesejada = 3;
    else if (seguimento.raioMedio <= 50)
        velocidadeDesejada = 4;
    else if (seguimento.raioMedio <= 80)
        velocidadeDesejada = 5;
    else if (seguimento.raioMedio <= 100)
        velocidadeDesejada = 6.5;
    else if (seguimento.raioMedio <= 150)
        velocidadeDesejada = 7;
    else if (seguimento.raioMedio <= 200)
        velocidadeDesejada = 9;

    return velocidadeDesejada;

}


function calculaVelocidade(nomeCarro) {

    if (nomeCarro == undefined)
        nomeCarro = botName;

    if (posicoesCarros.length > 1) { //Se houver apenas uma posi��o guardada ent�o ainda n�o h� como calcular velocidade

        var posicaoAnterior = retornaPosicaoCarroPorNome(posicoesCarros[posicoesCarros.length - 2].data, nomeCarro);
        var posicaoAtual = retornaPosicaoCarroPorNome(posicoesCarros[posicoesCarros.length - 1].data, nomeCarro);

        if (posicaoAtual.piecePosition.pieceIndex == posicaoAnterior.piecePosition.pieceIndex) { //Dentro de uma mesma peca somente subiraio a posicao atual da anterior.

            return posicaoAtual.piecePosition.inPieceDistance - posicaoAnterior.piecePosition.inPieceDistance

        } else { //Se trocou de peca dai complica, tem que calcular o quanto andou na pe�a anterior e somar com a atual

            var pecaAnterior = corrida.track.pieces[posicaoAnterior.piecePosition.pieceIndex];

            var distanciaAnterior = 0;

            if (pecaAnterior.angle == undefined) { //Se a pe�a anterior � uma reta ent�o � s� subitrair

                distanciaAnterior = pecaAnterior.length - posicaoAnterior.piecePosition.inPieceDistance;

            } else { //Se a pe�a anterior � uma curva dai tem que calcular atrav�s dao angulo

                var distanciaTotalAnterior = calculaDistanciaCurva(pecaAnterior.angle, pecaAnterior.radius, retornaTrilhaQueCarroEsta(nomeCarro));

                distanciaAnterior = distanciaTotalAnterior - posicaoAnterior.piecePosition.inPieceDistance;

            }

            return distanciaAnterior + posicaoAtual.piecePosition.inPieceDistance;

        }

    } else {
        return 0;
    }

}


function calculaDistanciaCurva(angulo, raio, trilhaCarro) {

    var distanciaCentroPista = retornaDistanciaTrilhaDoCentroDaPista(trilhaCarro);

    distanciaCentroPista = ajustaSinalDistanciaDoCentroDaPista(distanciaCentroPista, angulo);

    raio = raio + distanciaCentroPista;

    var perimetroCirculo = (3.14159265359 * raio * 2);

    return perimetroCirculo * (Math.abs(angulo) / 360.0);

}

function retornaTrilhaQueCarroEsta(nomeCarro) {
    for (i = 0; i < trilhasCarros[trilhasCarros.length - 1].length; ++i) {
        if (trilhasCarros[trilhasCarros.length - 1][i].nome == nomeCarro)
            return trilhasCarros[trilhasCarros.length - 1][i].trilha;
    }
}

function retornaDistanciaTrilhaDoCentroDaPista(trilhaCarro) {
    for (i = 0; i < corrida.track.lanes.length; ++i)
        if (corrida.track.lanes[i].index == trilhaCarro)
            return corrida.track.lanes[i].distanceFromCenter;
}

//Calcula o valor q deve ser adicionado ao raio para calcular o quanto um carro andou. Levando em considera��o se a curva � para direita ou esquerda
//TODO: encontrar um nome melhor pra essa fun��o
function ajustaSinalDistanciaDoCentroDaPista(distanciaCentroPista, angulo) {
    if (distanciaCentroPista < 0) { //trilha da esquerda
        if (angulo > 0) //curva para esquerda
            distanciaCentroPista = distanciaCentroPista * -1; //� meio redundante mas fica mais claro fazer com o else
    } else {
        if (angulo < 0) //curva para esquerda
            distanciaCentroPista = distanciaCentroPista * -1;
    }
    return distanciaCentroPista;
}



function retornaPosicaoCarroPorNome(carros, Nome) {

    for (i = 0; i < carros.length; ++i) {
        if (carros[i].id.name == Nome) {
            return carros[i];
        }
    }

}

function populaTrilhasCarros(carPositions) {

    var carros = carPositions.data;

    var trilhaCarro = new Array();

    for (i = 0; i < carros.length; ++i) {

        var posicao = {
            nome: carros[i].id.name,
            trilha: carros[i].piecePosition.lane.endLaneIndex
        }

        trilhaCarro[trilhaCarro.length] = posicao;

    }

    trilhasCarros[trilhasCarros.length] = trilhaCarro;

}





function populaSeguimentosCorrida() {


    for (index = 0; index < corrida.track.pieces.length; ++index) {

        if (pecaAnteriorDiferenteDaAtual(index, corrida)) {

            seguimentosCorrida[seguimentosCorrida.length] = {
                tipo: "",
                pecas: new Array()
            };

            if (corrida.track.pieces[index].angle == undefined) {

                seguimentosCorrida[seguimentosCorrida.length - 1].tipo = "reta";

            } else if (corrida.track.pieces[index].angle > 0) {

                seguimentosCorrida[seguimentosCorrida.length - 1].tipo = "curva";
                seguimentosCorrida[seguimentosCorrida.length - 1].curvaDierecao = "direita";

            } else if (corrida.track.pieces[index].angle < 0) {

                seguimentosCorrida[seguimentosCorrida.length - 1].tipo = "curva";
                seguimentosCorrida[seguimentosCorrida.length - 1].curvaDierecao = "esquerda";

            }

        }

        seguimentosCorrida[seguimentosCorrida.length - 1].pecas[seguimentosCorrida[seguimentosCorrida.length - 1].pecas.length] = corrida.track.pieces[index];

    }

    //Populando algulo  m�dio e raio
    for (i = 0; i < seguimentosCorrida.length; ++i) {

        if (seguimentosCorrida[i].tipo == "curva") {
            var qtd = 0;
            var raio = 0;
            var angulo = 0;
            var distancia = 0;
            for (y = 0; y < seguimentosCorrida[i].pecas.length; ++y) {
                qtd += 1;
                angulo += seguimentosCorrida[i].pecas[y].angle;
                raio += seguimentosCorrida[i].pecas[y].radius;
                distancia += (3.14159265359 * seguimentosCorrida[i].pecas[y].radius * 2) * (Math.abs(seguimentosCorrida[i].pecas[y].angle) / 360.0);
            }
            seguimentosCorrida[i].raioMedio = raio / qtd;
            seguimentosCorrida[i].anguloMedio = angulo / qtd;
            seguimentosCorrida[i].distancia = distancia;
        } else {
            var distancia = 0;
            for (y = 0; y < seguimentosCorrida[i].pecas.length; ++y) {
                distancia += seguimentosCorrida[i].pecas[y].length;
            }
            seguimentosCorrida[i].distancia = distancia;
        }
    }

    escrever(JSON.stringify(seguimentosCorrida));
}

function pecaAnteriorDiferenteDaAtual(index, corrida) {

    if (index == 0) {
        return true;
    }
    else {

        if (corrida.track.pieces[index].angle != undefined) {

            if (corrida.track.pieces[index - 1].angle != undefined) {

                if (corrida.track.pieces[index].angle > 0) {

                    if (corrida.track.pieces[index - 1].angle > 0) {

                        return false;

                    } else {

                        return true;

                    }


                } else {

                    if (corrida.track.pieces[index - 1].angle < 0) {

                        return false;

                    } else {

                        return true;

                    }

                }

            }
            else {
                return true;
            }

        }
        else {

            if (corrida.track.pieces[index - 1].angle != undefined) {
                return true;
            } else {
                return false;
            }
        }
    }

}

function retornaSeguimento(indice) {

    var posicaoAtual = retornaPosicaoCarroPorNome(posicoesCarros[posicoesCarros.length - 1].data, botName);
    var indexPecasAux = 0;

    for (indiceRetorno = 0; indiceRetorno < seguimentosCorrida.length; ++indiceRetorno) {

        if (seguimentosCorrida[indiceRetorno].pecas.length + indexPecasAux > posicaoAtual.piecePosition.pieceIndex) {

            if (seguimentosCorrida[indiceRetorno + indice] == undefined)
                return seguimentosCorrida[indice - 1];
            else if (indiceRetorno + indice < 0)
                return seguimentosCorrida[seguimentosCorrida.length - 1];
            else
                return seguimentosCorrida[indiceRetorno + indice];

        } else {

            indexPecasAux += seguimentosCorrida[indiceRetorno].pecas.length;

        }

    }


}

function retornaPorcentagemPercorridaSeguimento() {

    return retornaIndicePecaAtualNoContextoDoSeguimento() / retornaSeguimento(0).pecas.length;

}

function retornaIndicePecaAtualNoContextoDoSeguimento() {

    var posicaoAtual = retornaPosicaoCarroPorNome(posicoesCarros[posicoesCarros.length - 1].data, botName);
    var indexPecasAux = 0;

    for (indiceRetorno = 0; indiceRetorno < seguimentosCorrida.length; ++indiceRetorno) {

        if (seguimentosCorrida[indiceRetorno].pecas.length + indexPecasAux > posicaoAtual.piecePosition.pieceIndex) {

            return (posicaoAtual.piecePosition.pieceIndex - indexPecasAux);

        } else {

            indexPecasAux += seguimentosCorrida[indiceRetorno].pecas.length;

        }

    }
}

function retornaQuantoFaltaParaProximoSeguimento() {

    var seguimentoAtual = retornaSeguimento(0);
    var posicaoAtual = retornaPosicaoCarroPorNome(posicoesCarros[posicoesCarros.length - 1].data, botName);
    var distanciaPercorridaPeca = posicaoAtual.piecePosition.inPieceDistance;

    var retorno = 0;
    if (seguimentoAtual.tipo == "reta") {

        for (i = retornaIndicePecaAtualNoContextoDoSeguimento() + 1; i < seguimentoAtual.pecas.length; ++i)
            retorno += seguimentoAtual.pecas[i].length;

        retorno += corrida.track.pieces[posicaoAtual.piecePosition.pieceIndex].length - posicaoAtual.piecePosition.inPieceDistance;


    } else {

        for (y = retornaIndicePecaAtualNoContextoDoSeguimento() + 1; y < seguimentoAtual.pecas.length; ++y) {
            retorno += calculaDistanciaCurva(seguimentoAtual.pecas[y].angle, seguimentoAtual.pecas[y].radius, retornaTrilhaQueCarroEsta(botName));
        }
        var tamanhoPecaAtual = calculaDistanciaCurva(corrida.track.pieces[posicaoAtual.piecePosition.pieceIndex].angle, corrida.track.pieces[posicaoAtual.piecePosition.pieceIndex].radius, retornaTrilhaQueCarroEsta(botName))

        retorno += tamanhoPecaAtual - posicaoAtual.piecePosition.inPieceDistance;

    }

    return retorno;

}

function escrever(valor) {
    fs.writeFile("C:\\Users\\Samuel\\Desktop\\log.txt", valor + "\r\n", { flag: "a" }, function (err) {
        if (err) {
            console.log(err);
        } else {

        }
    });
}